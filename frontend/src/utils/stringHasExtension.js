/**
 * Function to check if a string has an extension in one of the allowedExtensions list
 *
 * @export
 * @param {string} str
 * @param {Array} allowedExtensions
 * @returns
 */
export default function stringHasExtension(str, allowedExtensions) {
  if (typeof str !== "string") {
    // eslint-disable-next-line no-console
    console.error(
      "The string provided to the the stringHasExtension function is not a string..."
    );
    return false;
  }
  if (!str) {
    return false;
  }

  const strExtension = str.split(".").slice(-1)[0].toLowerCase();

  return allowedExtensions.some((ext) => ext.toLowerCase() === strExtension);
}

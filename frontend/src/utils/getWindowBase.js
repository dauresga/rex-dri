function getWindowBase() {
  return window.location.href.replace(/\/app.*/, "");
}

export default getWindowBase;

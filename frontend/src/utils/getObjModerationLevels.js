import defaults from "../../../backend/backend_app/settings/defaults.yml";

const OBJ_MODERATION_PERMISSIONS = defaults.DEFAULT_OBJ_MODERATION_PERMISSIONS;
/**
 * Function to get the moderation levels available
 * if restrict is true then only the levels that are <= to the one of the user
 * are returned
 *
 * @export
 * @param {number} userLevel
 * @param {boolean} [restrict=false]
 * @returns {object}
 */
export default function getObjModerationLevel(userLevel, restrict = false) {
  const out = [];
  Object.keys(OBJ_MODERATION_PERMISSIONS).forEach((key) => {
    const { label } = OBJ_MODERATION_PERMISSIONS[key];
    const value = OBJ_MODERATION_PERMISSIONS[key].level;
    if (!restrict || (restrict && value <= userLevel)) {
      out.push({
        label,
        value,
        disabled: value > userLevel,
      });
    }
  });
  return out;
}

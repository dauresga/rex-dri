import React from "react";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/styles";
import classNames from "../../utils/classNames";

export function getLegendColorOpacity(theme, selected) {
  const color = selected
    ? theme.palette.primary.main
    : theme.palette.action.disabled;
  const opacity = selected ? 0.8 : 0.5;
  return { color, opacity };
}

const useStyles = makeStyles((theme) => {
  const {
    color: colorSelected,
    opacity: opacitySelected,
  } = getLegendColorOpacity(theme, true);
  const {
    color: colorNotSelected,
    opacity: opacityNotSelected,
  } = getLegendColorOpacity(theme, false);
  const circleSize = theme.typography.caption.fontSize;

  return {
    legendContainer: {
      padding: theme.spacing(1),
      backgroundColor: theme.palette.background.default,
      borderRadius: theme.spacing(1),
    },
    legendItemContainer: {
      display: "flex",
      alignItems: "center",
      padding: theme.spacing(0.5),
    },
    legendItemCircle: {
      borderRadius: circleSize,
      width: circleSize,
      height: circleSize,
      marginRight: theme.spacing(0.5),
    },
    selectedLegendItemCircle: {
      backgroundColor: colorSelected,
      opacity: opacitySelected,
    },
    notSelectedLegendItemCircle: {
      backgroundColor: colorNotSelected,
      opacity: opacityNotSelected,
    },
  };
});

function LegendItem({ selected, label }) {
  const classes = useStyles();

  return (
    <div className={classes.legendItemContainer}>
      <div
        className={classNames(
          classes.legendItemCircle,
          selected
            ? classes.selectedLegendItemCircle
            : classes.notSelectedLegendItemCircle
        )}
      />
      <Typography variant="caption">{label}</Typography>
    </div>
  );
}

LegendItem.propTypes = {
  label: PropTypes.string.isRequired,
  selected: PropTypes.bool.isRequired,
};

LegendItem.defaultProps = {};

/**
 * Custom component to create the legend on the map
 */
function Legend() {
  const classes = useStyles();

  return (
    <div className={classes.legendContainer}>
      <LegendItem label="L'univ. correspond aux filtres" selected />
      <LegendItem label="Autres universités" selected={false} />
    </div>
  );
}

Legend.propTypes = {};

Legend.defaultProps = {};

export default Legend;

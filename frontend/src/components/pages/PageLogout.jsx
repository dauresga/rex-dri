import React from "react";
import PropTypes from "prop-types";
import { Typography } from "@material-ui/core";
import APP_ROUTES from "../../config/appRoutes";

let _triggered = false;

/**
 * Component to force user logout
 */
function PageLogout(props) {
  if (!_triggered) {
    _triggered = true;
    props.history.push(APP_ROUTES.logout);
    window.location.reload(); // need to reload otherwise it might not work on mobile
  }

  return <Typography variant="caption">Déconnexion on cours...</Typography>;
}

PageLogout.propTypes = {
  history: PropTypes.object.isRequired,
};

export default PageLogout;

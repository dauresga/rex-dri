import React from "react";
import PropTypes from "prop-types";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import { makeStyles } from "@material-ui/styles";
import CustomError from "../../common/CustomError";

const useStyles = makeStyles((theme) => ({
  formElement: {
    marginBottom: theme.spacing(3),
  },
}));

/**
 * Wrapper for fields in forms to handle labels and visual feedback
 */
function FieldWrapper({ required, errorObj, label, children, commentText }) {
  const classes = useStyles();

  return (
    <FormControl
      className={classes.formElement}
      required={required}
      error={errorObj.status}
      fullWidth
    >
      <FormLabel>{label}</FormLabel>
      {commentText ? <FormHelperText>{commentText}</FormHelperText> : <></>}
      {errorObj.messages.map((error, idx) => (
        // eslint-disable-next-line react/no-array-index-key
        <FormHelperText key={idx}>{error}</FormHelperText>
      ))}
      {children}
    </FormControl>
  );
}

FieldWrapper.defaultProps = {
  label: "LABEL",
  required: false,
  errorObj: new CustomError(),
  commentText: "",
};

FieldWrapper.propTypes = {
  label: PropTypes.string,
  commentText: PropTypes.string,
  required: PropTypes.bool,
  errorObj: PropTypes.instanceOf(CustomError), // Error state of the field
  children: PropTypes.node.isRequired,
};

export default FieldWrapper;

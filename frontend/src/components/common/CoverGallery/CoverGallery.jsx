import React from "react";
import { makeStyles } from "@material-ui/styles";
// import withStyles from "@material-ui/core/styles/withStyles";
// import { compose } from "recompose";
// import PropTypes from "prop-types";
// import shuffle from "lodash/shuffle";
// import Typography from "@material-ui/core/Typography";
// import { AwesomeSlider } from "react-awesome-slider";
// import styles from "./CoverGallery.scss";
//
// /**
//  * Component to display a cover image gallery with custom styling done in the ./CoverGallery.scss file
//  * If no image is provided then a replacement is added :)
//  */
// function CoverGallery({ classes, picturesSrc }) {
//   return (
//     <div
//       style={{
//         position: "relative",
//         width: "100%",
//       }}
//     >
//       {picturesSrc.length === 0 ? (
//         <div className={classes.missingPictureDiv}>
//           {/* <PhotoSizeSelectActualIcon className={classes.missingPictureIcon}/> */}
//         </div>
//       ) : (
//         <AwesomeSlider cssModule={styles}>
//           {shuffle(picturesSrc).map((src) => (
//             <div key={src} data-src={src} />
//           ))}
//           <div data-src="https://www.stcc.ch/wp-content/uploads/2017/11/epfl.jpg" />
//         </AwesomeSlider>
//       )}
//
//       <div className={classes.editButton}>
//         <Typography variant="caption">
//           Les photos de couverture ne sont pas encore disponibles.
//         </Typography>
//         {/* <Button variant={"contained"} */}
//         {/*        color="default" */}
//         {/*        onClick={() => onEditButtonClick()}> */}
//         {/*  <span className={classes.editButtonLabel}>Éditer les photos</span> */}
//         {/*  <CreateIcon className={classes.rightIcon}/> */}
//         {/* </Button> */}
//       </div>
//     </div>
//   );
// }
//
// CoverGallery.propTypes = {
//   classes: PropTypes.object.isRequired,
//   picturesSrc: PropTypes.arrayOf(PropTypes.string),
//   // eslint-disable-next-line react/no-unused-prop-types
//   onEditButtonClick: PropTypes.func,
// };
//
// CoverGallery.defaultProps = {
//   picturesSrc: [],
//   onEditButtonClick: () =>
//     // eslint-disable-next-line no-console
//     console.log("No function provided to edit cover gallery"),
// };
//
// const componentStyles = (theme) => ({
//   missingPictureDiv: {
//     height: "27vh",
//     maxHeight: 150,
//     background: `repeating-linear-gradient(
//         45deg,
//         ${theme.palette.background.default},
//         ${theme.palette.background.default} 10px,
//         ${theme.palette.background.paper} 10px,
//         ${theme.palette.background.paper} 20px
//       )`,
//   },
//   missingPictureIcon: {
//     marginLeft: "auto",
//     marginRight: "auto",
//     width: "100%",
//     height: "100%",
//   },
//   editButton: {
//     position: "absolute",
//     zIndex: 1000,
//     right: theme.spacing(2),
//     bottom: theme.spacing(2),
//   },
//   editButtonLabel: {
//     [theme.breakpoints.down("sm")]: {
//       display: "none",
//     },
//   },
//   rightIcon: {
//     [theme.breakpoints.up("md")]: {
//       marginLeft: theme.spacing(1),
//     },
//   },
// });
//
// export default compose(withStyles(componentStyles, { withTheme: true }))(
//   CoverGallery
// );

const useStyles = makeStyles((theme) => ({
  dummyDiv: {
    height: theme.spacing(2),
    background: "transparent",
  },
}));

function DummyCoverGallery() {
  const classes = useStyles();
  return <div className={classes.dummyDiv} />;
}

export default DummyCoverGallery;

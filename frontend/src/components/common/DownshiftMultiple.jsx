// Inspired by : https://material-ui.com/demos/autocomplete/

import React, { useCallback, useEffect, useMemo, useState } from "react";
import PropTypes from "prop-types";
import keycode from "keycode";
import Downshift from "downshift";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import MenuItem from "@material-ui/core/MenuItem";
import Chip from "@material-ui/core/Chip";
import fuzzysort from "fuzzysort";
import { makeStyles } from "@material-ui/styles";
import useGlobalState from "../../hooks/useGlobalState";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  container: {
    flexGrow: 1,
    position: "relative",
  },
  paper: {
    position: "absolute",
    zIndex: 200000,
    marginTop: theme.spacing(1),
    left: 0,
    right: 0,
  },
  chip: {
    margin: theme.spacing(0.5, 0.25),
  },
  inputRoot: {
    flexWrap: "wrap",
  },
  notSelectedSuggestion: {
    fontWeight: 400,
  },
  selectedSuggestion: {
    fontWeight: 500,
  },
}));

function Input({ inputProps, autoFocus }) {
  const classes = useStyles();

  const { InputProps, ref, ...other } = inputProps;

  return (
    <TextField
      autoFocus={autoFocus}
      InputProps={{
        inputRef: ref,
        classes: { root: classes.inputRoot },
        ...InputProps,
      }}
      {...other}
    />
  );
}

Input.propTypes = {
  inputProps: PropTypes.object.isRequired,
  autoFocus: PropTypes.bool.isRequired,
};

function Suggestion({
  suggestion,
  index,
  itemProps,
  highlightedIndex,
  selectedItem,
}) {
  const classes = useStyles();
  const isHighlighted = highlightedIndex === index;
  const isSelected = (selectedItem || "").indexOf(suggestion) > -1;

  return (
    <MenuItem
      {...itemProps}
      selected={isHighlighted}
      component="div"
      className={
        isSelected ? classes.selectedSuggestion : classes.notSelectedSuggestion
      }
    >
      {suggestion.label}
    </MenuItem>
  );
}

Suggestion.propTypes = {
  highlightedIndex: PropTypes.number,
  index: PropTypes.number.isRequired,
  itemProps: PropTypes.object.isRequired,
  selectedItem: PropTypes.string,
  suggestion: PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string,
  }).isRequired,
};

Suggestion.defaultProps = {
  highlightedIndex: null,
  selectedItem: null,
};

function DownshiftMultiple({
  cacheId,
  value: valueFromProps,
  inputValue: inputValueFromProps,
  options: optionsFromProps,
  onChange: onChangeFromProps,
  multiple,
  fieldLabel,
  fieldPlaceholder,
  autoFocusInput,
}) {
  const reverseOptions = useMemo(() => {
    const out = new Map();
    optionsFromProps.forEach((el) => out.set(el.value, el.label));
    return out;
  }, [optionsFromProps]);

  const [cache, setCache] = useGlobalState(`app-downshift-${cacheId}`, {
    value: valueFromProps,
    inputValue: inputValueFromProps,
  });

  const [value, setValue] = useState(
    cacheId === null ? [...valueFromProps] : [...cache.value]
  );
  const [inputValue, setInputValue] = useState(
    cacheId === null ? inputValueFromProps : cache.inputValue
  );

  // saving also to cache
  useEffect(() => {
    if (cacheId !== null) setCache({ value, inputValue });
  }, [value, inputValue]);

  const options = useMemo(() => {
    return optionsFromProps.map((el) => ({
      value: el.value,
      label: el.label,
      valueStr: el.value.toString(),
    }));
  }, [optionsFromProps]);

  const onChange = useCallback(
    (newValue) => {
      if (multiple) {
        onChangeFromProps(newValue);
      } else {
        // we return only one value or null
        onChangeFromProps(newValue.length === 0 ? null : newValue[0]);
      }
    },
    [onChangeFromProps]
  );

  const getSuggestions = useCallback(
    (newInputValue) => {
      // Suggest only unselected options
      const possible = options.filter((el) => !value.includes(el.value));

      const filter = fuzzysort.go(newInputValue, possible, {
        limit: 5,
        keys: ["label", "valueStr"],
      });
      if (filter.length > 0) {
        return filter.map((item) => item.obj);
      }
      return possible.slice(0, 4);
    },
    [value, options]
  );

  const handleKeyDown = useCallback(
    (event) => {
      if (
        value.length &&
        !inputValue.length &&
        keycode(event) === "backspace"
      ) {
        setValue((v) => v.slice(0, v.length - 1));
      }
    },
    [value, inputValue]
  );

  const handleInputChange = useCallback((event) => {
    setInputValue(event.target.value);
  }, []);

  const handleChange = useCallback(
    (val) => {
      if (val === null) return; // on reset will be null

      let newValue = value;
      if (value.indexOf(val) === -1) {
        newValue = [...value, val];
        if (!multiple) {
          newValue =
            newValue.length === 0 ? [] : [newValue[newValue.length - 1]];
        }
        // Tell subscriber
        onChange(newValue);
      }
      setInputValue("");
      setValue(newValue);
    },
    [value]
  );

  const handleDelete = useCallback(
    (val) => {
      setValue((valueInState) => {
        const newValue = valueInState.filter((v) => val !== v);
        onChange(newValue);
        return newValue;
      });
    },
    [onChange]
  );

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Downshift inputValue={inputValue} onChange={handleChange}>
        {({
          getInputProps,
          getItemProps,
          isOpen,
          inputValue: inputValue2,
          selectedItem: selectedItem2,
          highlightedIndex,
          clearSelection,
        }) => (
          <div className={classes.container}>
            <Input
              autoFocus={autoFocusInput}
              inputProps={{
                fullWidth: true,
                InputProps: getInputProps({
                  startAdornment: value.map((val) => (
                    <Chip
                      key={val}
                      tabIndex={-1}
                      label={reverseOptions.get(val)}
                      className={classes.chip}
                      onDelete={() => {
                        handleDelete(val);
                        clearSelection();
                      }}
                      variant="outlined"
                      color="primary"
                    />
                  )),
                  onChange: handleInputChange,
                  onKeyDown: handleKeyDown,
                  placeholder: fieldPlaceholder,
                }),
                label: fieldLabel,
              }}
            />
            {isOpen ? (
              <Paper className={classes.paper} square>
                {getSuggestions(inputValue2).map((suggestion, index) => (
                  <Suggestion
                    key={suggestion.value}
                    highlightedIndex={highlightedIndex}
                    selectedItem={selectedItem2}
                    suggestion={suggestion}
                    index={index}
                    itemProps={getItemProps({ item: suggestion.value })}
                  />
                ))}
              </Paper>
            ) : null}
          </div>
        )}
      </Downshift>
    </div>
  );
}

DownshiftMultiple.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  ), // only used on mount // the initial value must be provided as an array,
  inputValue: PropTypes.string, // only used on mount
  autoFocusInput: PropTypes.bool,
  fieldLabel: PropTypes.string,
  fieldPlaceholder: PropTypes.string,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([
        PropTypes.string.isRequired,
        PropTypes.number.isRequired,
      ]).isRequired, // NEVER PUT A NULL
    })
  ).isRequired,
  multiple: PropTypes.bool, // do we allow multiple values to be selected.
  cacheId: PropTypes.string, // if not null enable caching of value and input Value
  // If multiple is true than null or one value in returned
};

DownshiftMultiple.defaultProps = {
  autoFocusInput: false,
  fieldLabel: "",
  fieldPlaceholder: "Placeholder",
  multiple: true,
  value: [],
  inputValue: "",
  cacheId: null,
};

export default React.memo(DownshiftMultiple);

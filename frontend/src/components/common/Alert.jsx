import React from "react";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  multilineButton: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
  },
  button: {},
});

/**
 * Component to render an "alert" that prevents all other interaction on the site.
 */
function Alert({
  open,
  title,
  description,
  info,
  handleClose,
  handleResponse,
  infoText,
  multilineButtons,
  disagreeText,
  agreeText,
}) {
  const classes = useStyles();

  if (!open) {
    return <></>;
  }

  return (
    <Dialog open>
      <>
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
          <DialogContentText style={{ whiteSpace: "pre-wrap" }}>
            {description}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          {info ? (
            <Button
              onClick={() => {
                handleClose();
                handleResponse();
              }}
              color="primary"
            >
              {infoText}
            </Button>
          ) : (
            <div>
              <Button
                onClick={() => {
                  handleClose();
                  handleResponse(false);
                }}
                color="secondary"
                className={
                  multilineButtons ? classes.multilineButton : classes.button
                }
              >
                {disagreeText}
              </Button>
              <Button
                onClick={() => {
                  handleClose();
                  handleResponse(true);
                }}
                className={
                  multilineButtons ? classes.multilineButton : classes.button
                }
                variant="outlined"
                color="primary"
                autoFocus
              >
                {agreeText}
              </Button>
            </div>
          )}
        </DialogActions>
      </>
    </Dialog>
  );
}

Alert.propTypes = {
  open: PropTypes.bool, // is the alert open
  title: PropTypes.string, // Title display on the vindow
  description: PropTypes.string, // textual description (below the titile)
  info: PropTypes.bool, // If it's just un info alert with one button
  infoText: PropTypes.string, // content of the alert when it's an info
  agreeText: PropTypes.string, // Text display in the "agree" button
  disagreeText: PropTypes.string, // Text display in the "disagree" button
  multilineButtons: PropTypes.bool, // should the agree/disagree button displayed on multiple lines
  handleClose: PropTypes.func, // function called when the alert is closed
  handleResponse: PropTypes.func, // function called allong the previous one with false if the user disagreed and true otherwise. Or no parameters if info.
};

Alert.defaultProps = {
  open: false,
  title: "Mon titre",
  description: "",
  info: false,
  agreeText: "OK",
  disagreeText: "Annuler",
  infoText: "J'ai compris",
  multilineButtons: false,
  handleClose: () => {
    // eslint-disable-next-line no-console
    console.error("Missing function for closing alert.");
  },
  handleResponse: () => {
    // eslint-disable-next-line no-console
    console.error("Missing function for handling post performClose.");
  },
};

export default Alert;

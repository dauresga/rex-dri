import React, { useEffect } from "react";
import PropTypes from "prop-types";
import AlertService from "../../services/AlertService";
import { useApiDelete } from "../../hooks/wrappers/api";

// TODO component is Useless...
function DeleteHandler({ performClose, route, id }) {
  const performDelete = useApiDelete(route);
  useEffect(() => {
    AlertService.open({
      info: false,
      title: "Confirmer la suppression de l'object.",
      description: "Ếtes-vous sûr⋅e ?",
      agreeText: "Oui",
      disagreeText: "Non",
      handleClose: { performClose },
      handleResponse: (confirmed) => {
        if (confirmed) performDelete(id, () => performClose());
        else performClose();
      },
    });
  }, []);

  return <></>;
}

DeleteHandler.propTypes = {
  route: PropTypes.string.isRequired,
  id: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.number.isRequired,
  ]).isRequired,
  performClose: PropTypes.func.isRequired,
};

export default DeleteHandler;

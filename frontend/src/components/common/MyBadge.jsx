import React from "react";
import Badge from "@material-ui/core/Badge";
import { withStyles } from "@material-ui/core";

// fix positioning of the badge that was a bit too high
const styles = (theme) => ({
  badge: {
    top: "8px",
    right: "9px",
    border: `1px solid ${theme.palette.background.paper}`,
  },
});

/**
 * Custom component to render a badge (little circle with a number in it) only if there is something interesting to display
 *
 * @export
 * @param {object} props
 * @returns
 */
export default withStyles(styles, { withTheme: true })((props) => {
  if (props.badgeContent >= props.minNumber) {
    return (
      <Badge
        classes={{ badge: props.classes.badge }}
        badgeContent={props.badgeContent}
        color={props.color}
        max={5}
      >
        {props.children}
      </Badge>
    );
  }
  return props.children;
});

import React from "react";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/styles";
import TextLink from "./TextLink";
import APP_ROUTES from "../../config/appRoutes";
import getWindowBase from "../../utils/getWindowBase";

const useStyles = makeStyles((theme) => ({
  spacer: {
    marginBottom: theme.spacing(2),
  },
}));

/**
 * Component to render the links in custom manner
 */
function LicenseNotice(props) {
  const classes = useStyles();

  return (
    <>
      <Typography variant="caption" display="block">
        Ce contenu est sous license&nbsp;
        {props.variant}.<br />
        Plus d'informations à ce propos sont disponibles&nbsp;
        <TextLink href={getWindowBase() + APP_ROUTES.aboutCgu}>ici</TextLink>.
      </Typography>
      {props.spacer && <div className={classes.spacer} />}
    </>
  );
}

LicenseNotice.propTypes = {
  variant: PropTypes.oneOf(["REX-DRI—PRIVATE", "REX-DRI—BY"]).isRequired,
  spacer: PropTypes.bool,
};

LicenseNotice.defaultProps = {
  spacer: true,
};

export default LicenseNotice;

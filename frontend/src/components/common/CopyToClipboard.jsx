import Tooltip from "@material-ui/core/Tooltip";
import React, { useState } from "react";
import PropTypes from "prop-types";
import clipboardCopy from "../../utils/clipboardCopy";

/**
 * Render prop component that wraps element in a Tooltip that shows "Copied to clipboard!" when the
 * copy function is invoked
 */
function CopyToClipboard({ message, render }) {
  const [showToolTip, setShowTooltip] = useState(false);

  return (
    <Tooltip
      open={showToolTip}
      title={message}
      leaveDelay={1500}
      onClose={() => setShowTooltip(false)}
    >
      {render((text) => {
        clipboardCopy(text);
        setShowTooltip(true);
      })}
    </Tooltip>
  );
}

CopyToClipboard.propTypes = {
  render: PropTypes.func.isRequired,
  message: PropTypes.string,
};

CopyToClipboard.defaultProps = {
  message: "Lien copié 😁",
};

export default React.memo(CopyToClipboard);

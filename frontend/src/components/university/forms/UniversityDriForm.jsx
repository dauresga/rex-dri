import React from "react";
import {
  CommentField,
  ImportanceLevelField,
  TitleField,
  UniversitiesField,
} from "../../edition/fields/wrappedFields";
import UsefulLinksField from "../../edition/fields/UsefulLinksField";
import FormInfo from "../../../utils/editionRelated/FormInfo";

function UniversityDriForm() {
  return (
    <>
      <TitleField />
      <ImportanceLevelField />
      <UniversitiesField />
      <CommentField />
      <UsefulLinksField />
    </>
  );
}

export default new FormInfo("universityDri", UniversityDriForm);

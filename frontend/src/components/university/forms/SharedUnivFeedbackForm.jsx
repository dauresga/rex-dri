import React from "react";

import Markdown from "../../common/markdown/Markdown";
import MarkdownField from "../../edition/fields/MarkdownField";
import HiddenField from "../../edition/fields/HiddenField";
import FormInfo from "../../../utils/editionRelated/FormInfo";

const infoSource = `
**NB :** pour toutes informations assez spécifiques vous avez l'onglet « Tips & Tricks » 
sur la page de l'université. Dans ce dernier vous pourrez mettre les commentaires qui s'adressent 
plutôt aux étudiantes et étudiants qui ont été sélectionnés pour partir en échange.
`;

function SharedUnivFeedbackForm() {
  return (
    <>
      <HiddenField fieldMapping="university" />
      <Markdown source={infoSource} />
      <MarkdownField
        fieldMapping="comment"
        maxLength={5000}
        label="Commentaire sur l'université partagé par tous les étudiants s'y étant rendu"
      />
    </>
  );
}

export default new FormInfo("sharedUnivFeedbacks", SharedUnivFeedbackForm);

export default function getVersionTooltipAndClass(nbVersions) {
  let versionTooltip, versionClass;

  if (typeof nbVersions === "undefined" || isNaN(nbVersions)) {
    versionTooltip = "Ce contenu n'est pas versionné.";
    versionClass = "disabled";
  } else {
    switch (nbVersions) {
      case 0:
      case 1:
        versionTooltip = "Il n'existe pas d'autres versions de ce contenu.";
        versionClass = "disabled";
        break;
      case 2:
        versionTooltip =
          "Le contenu actuel constitue la première mise à jour de ce module, vous pouvez voir la version initiale en cliquant sur cette icône.";
        versionClass = "green";
        break;
      default:
        versionTooltip = `Le contenu actuel constitue la ${nbVersions}ème mise à jour de ce module, vous pouvez voir les versions précédentes en cliquant sur cette icône.`;
        versionClass = "green";
        break;
    }
  }

  return {
    versionTooltip,
    versionClass,
  };
}

import React from "react";
import PropTypes from "prop-types";
import Paper from "@material-ui/core/Paper";
import Divider from "@material-ui/core/Divider";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Chip from "@material-ui/core/Chip";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import IconButton from "@material-ui/core/IconButton";
import CreateIcon from "@material-ui/icons/Create";
import CourseFeedback from "./CourseFeedback";
import MetricFeedback from "../../../common/MetricFeedback";
import TruncatedMarkdown from "../../../common/markdown/TruncatedMarkdown";
import APP_ROUTES from "../../../../config/appRoutes";
import LinkToUser from "../../../common/LinkToUser";
import { GridColumn, GridLine, useStyle } from "./common";
import NavigationService from "../../../../services/NavigationService";

const metricProps = {
  min: -5,
  max: 5,
};

export function GeneralFeedbackCore(props) {
  const classes = useStyle();

  if (props.untouched)
    return (
      <Typography>
        <em>Aucun avis saisi.</em>
      </Typography>
    );

  return (
    <>
      <GridLine>
        <GridColumn>
          <Typography variant="h6" className={classes.feedbackHeaderText}>
            Niveau académique
          </Typography>
          <MetricFeedback {...metricProps} value={props.academicalLevel} />
        </GridColumn>
        <GridColumn>
          <Typography variant="h6" className={classes.feedbackHeaderText}>
            Accueil des étudiants étrangers
          </Typography>
          <MetricFeedback
            {...metricProps}
            value={props.foreignStudentWelcome}
          />
        </GridColumn>
        <GridColumn>
          <Typography variant="h6" className={classes.feedbackHeaderText}>
            Intérêt culturel
          </Typography>
          <MetricFeedback {...metricProps} value={props.culturalInterest} />
        </GridColumn>
      </GridLine>
      <Divider />

      <Typography variant="h6">Avis général</Typography>
      <TruncatedMarkdown source={props.generalComment} />
    </>
  );
}

GeneralFeedbackCore.propTypes = {
  academicalLevel: PropTypes.number.isRequired,
  foreignStudentWelcome: PropTypes.number.isRequired,
  culturalInterest: PropTypes.number.isRequired,
  generalComment: PropTypes.string,
  untouched: PropTypes.bool.isRequired,
};

GeneralFeedbackCore.defaultProps = {
  generalComment: "",
};

function PreviousExchange({ rawModelData }) {
  const classes = useStyle();
  const cleanSemester = `${rawModelData.exchange.semester.toUpperCase()} ${
    rawModelData.exchange.year
  }`;
  const canEdit = rawModelData.obj_info.user_can_edit;
  const courses = rawModelData.exchange.exchange_courses;
  const isWide = useMediaQuery("(min-width:1100px)");

  function renderCore() {
    return (
      <>
        {!rawModelData.untouched && (
          <GeneralFeedbackCore
            academicalLevel={rawModelData.academical_level_appreciation}
            foreignStudentWelcome={rawModelData.foreign_student_welcome}
            culturalInterest={rawModelData.cultural_interest}
            generalComment={rawModelData.general_comment}
            untouched={rawModelData.untouched}
          />
        )}
        {courses.length !== 0 && (
          <ExpansionPanel
            className={classes.expansionPanel}
            elevation={4}
            TransitionProps={{ unmountOnExit: true }}
          >
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography variant="h6">Évaluation des cours suivis</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails style={{ display: "block" }}>
              <CourseFeedback exchangeCourses={courses} />
            </ExpansionPanelDetails>
          </ExpansionPanel>
        )}
      </>
    );
  }

  const updaterId = rawModelData.exchange.student.user_id;
  const updaterUseName = rawModelData.exchange.student.user_goes_by;

  const majorSemester = rawModelData.exchange.student_major_and_semester;
  const minor = rawModelData.exchange.student_minor;

  return (
    <Paper className={classes.paper}>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <div>
          <Chip
            label={cleanSemester}
            className={classes.chip}
            color="primary"
          />
          <Chip
            label={`${majorSemester}${minor ? ` — ${minor}` : ""}`}
            className={classes.chip}
            color="secondary"
          />
          {updaterId && (
            <Typography display="inline">
              <LinkToUser userId={updaterId} pseudo={updaterUseName} />
            </Typography>
          )}
        </div>

        <div>
          &nbsp;
          {/* Needed to fire events for the tooltip when below is disabled!! */}
          {canEdit && (
            <IconButton
              color="primary"
              aria-label="Éditer"
              className={classes.editIcon}
              onClick={() =>
                NavigationService.goToRoute(
                  APP_ROUTES.editForPreviousExchange(rawModelData.id)
                )
              }
            >
              <CreateIcon />
            </IconButton>
          )}
        </div>
      </div>
      <Divider className={classes.spacer} />
      <div style={{ display: "flex" }}>
        <div className={isWide ? classes.leftColExtra : ""} />
        <div style={{ width: "100%" }}>{renderCore()}</div>
      </div>
    </Paper>
  );
}

PreviousExchange.propTypes = {
  rawModelData: PropTypes.object.isRequired,
};

export default PreviousExchange;

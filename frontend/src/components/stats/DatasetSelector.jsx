import React from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { makeStyles } from "@material-ui/styles";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  appBar: { zIndex: 1000 },
});

/**
 * Component to select the dataset
 */
function DatasetSelector({ datasets, value, onChange }) {
  const classes = useStyles();

  return (
    <>
      <Typography variant="h6">Choix du jeu de données</Typography>
      <AppBar
        color="default"
        position="relative"
        classes={{ root: classes.appBar }}
      >
        <Tabs
          value={value}
          onChange={(e, newDatasetName) => onChange(newDatasetName)}
          variant="standard"
          centered
          scrollButtons="on"
          indicatorColor="secondary"
          textColor="secondary"
        >
          {datasets.map(({ label, name }) => (
            <Tab label={label} value={name} key={name} />
          ))}
        </Tabs>
      </AppBar>
    </>
  );
}

DatasetSelector.propTypes = {
  datasets: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    })
  ).isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default DatasetSelector;

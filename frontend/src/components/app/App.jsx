/** General app JS entry
 */

import React from "react";
import { compose } from "recompose";
import { Route, Switch } from "react-router-dom";
import { withErrorBoundary } from "../common/ErrorBoundary";

import LastVisitedUniversity from "./LastVisitedUniversity";
import PageMap from "../pages/PageMap";
import PageHome from "../pages/PageHome";
import PageUniversity from "../pages/PageUniversity";
import PageSearch from "../pages/PageSearch";
import PageSettings from "../pages/PageThemeSettings";
import PageUser from "../pages/PageUser";
import PageLists from "../pages/PageLists";
import MainAppFrame from "./MainAppFrame";
import APP_ROUTES from "../../config/appRoutes";
import PageAboutProject from "../pages/PageAboutProject";
import { PageCgu, PageRgpd } from "../pages/PagesRgpdCgu";
import PageNotFound from "../pages/PageNotFound";
import PageEditPreviousExchanges from "../pages/PageEditExchangeFeedbacks";
import PageMyExchanges from "../pages/PageMyExchanges";
import FooterImportantInformation from "./FooterImportantInformation";
import PageAboutUnlinkedPartners from "../pages/PageAboutUnlinkedPartners";
import PageLogout from "../pages/PageLogout";
import withNetworkWrapper, { NetWrapParam } from "../../hoc/withNetworkWrapper";
import UniversityService from "../../services/data/UniversityService";
import CountryService from "../../services/data/CountryService";
import CurrencyService from "../../services/data/CurrencyService";
import LanguageService from "../../services/data/LanguageService";
import FilterService from "../../services/FilterService";
import useOnBeforeComponentMount from "../../hooks/useOnBeforeComponentMount";

const SERVICES_TO_INITIALIZE = [
  UniversityService,
  CountryService,
  CurrencyService,
  LanguageService,
  FilterService,
];

/**
 * Main entry
 */
function App() {
  useOnBeforeComponentMount(() => {
    SERVICES_TO_INITIALIZE.forEach((service) => service.initialize());
  });

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        justifyItems: "flex-end",
        minHeight: "100vh",
      }}
    >
      <MainAppFrame>
        <main>
          <Switch>
            <Route exact path={APP_ROUTES.base} component={PageHome} />
            <Route path={APP_ROUTES.search} component={PageSearch} />
            <Route path={APP_ROUTES.map} component={PageMap} />
            <Route path={APP_ROUTES.themeSettings} component={PageSettings} />
            <Route path={APP_ROUTES.listsWithParams} component={PageLists} />
            <Route
              path={APP_ROUTES.universityWithParams}
              component={PageUniversity}
            />
            <Route path={APP_ROUTES.myExchanges} component={PageMyExchanges} />
            <Route
              path={APP_ROUTES.editPreviousExchangeWithParams}
              component={PageEditPreviousExchanges}
            />
            <Route path={APP_ROUTES.userWithParams} component={PageUser} />
            {/* <Route path={APP_ROUTES.userFilesWithParams} component={PageFiles}/> WARNING BETA */}
            <Route
              path={APP_ROUTES.aboutProject}
              component={PageAboutProject}
            />
            <Route path={APP_ROUTES.aboutRgpd} component={PageRgpd} />
            <Route path={APP_ROUTES.aboutCgu} component={PageCgu} />
            <Route
              path={APP_ROUTES.aboutUnlinkedPartners}
              component={PageAboutUnlinkedPartners}
            />
            <Route path={APP_ROUTES.logout} component={PageLogout} />
            <Route component={PageNotFound} />
          </Switch>
        </main>
      </MainAppFrame>
      <LastVisitedUniversity />
      <FooterImportantInformation />
    </div>
  );
}

App.propTypes = {};

export default compose(
  withNetworkWrapper([
    new NetWrapParam("countries", "all"),
    new NetWrapParam("universities", "all"),
    new NetWrapParam("currencies", "all"),
    new NetWrapParam("languages", "all"),
    new NetWrapParam("serverModerationStatus", "all"), // not needed for server moderation status
  ]),
  withErrorBoundary()
)(App);

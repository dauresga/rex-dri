/**
 * React context to hold information related to the forms.
 */
import React from "react";

/**
 * @type {React.Context<{formManager: FormManager}>}
 */
const FormContext = React.createContext({ formManager: {} });
export default FormContext;

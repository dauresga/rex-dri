/**
 *
 * @param {boolean} isOpen - Inital state for open
 * @returns {[boolean, function, function]}
 */
import { useCallback, useState } from "react";

function useOpenClose(isOpen) {
  const [_isOpen, setIsOpen] = useState(isOpen);

  const open = useCallback(() => {
    setIsOpen(true);
  }, []);

  const close = useCallback(() => {
    setIsOpen(false);
  }, []);

  return [_isOpen, open, close];
}

export default useOpenClose;

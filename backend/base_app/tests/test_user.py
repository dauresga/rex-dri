import json

import pytest
from django.core.exceptions import ValidationError

from backend_app.tests.utils import WithUserTestCase
from base_app.models import UserViewset, User


class UserTestCase(WithUserTestCase):
    """
    Tests for the custom user class
    """

    @classmethod
    def setUpMoreTestData(cls):
        cls.api_users = "/api/{}/".format(UserViewset.end_point_route)

    def test_authentificated_can_be_anonymous(self):
        self.authenticated_user.allow_sharing_personal_info = True
        self.authenticated_user.save()
        self.authenticated_user.allow_sharing_personal_info = False
        self.authenticated_user.save()

    def test_moderators_cant_be_anonymous(self):
        self.moderator_user.allow_sharing_personal_info = True
        self.moderator_user.save()

        with pytest.raises(ValidationError):
            self.moderator_user.allow_sharing_personal_info = False
            self.moderator_user.save()

    def test_moderators_cant_be_anonymous_request(self):
        response = self.moderator_client.put(
            self.api_users + "{}/".format(self.moderator_user.pk),
            dict(allow_sharing_personal_info=False),
        )
        self.assertEqual(response.status_code, 400)

    def test_get_all_returns_empty_list(self):
        # check that we have indeed users in the db
        self.assertNotEqual(len(User.objects.all()), 0)

        # Check what is the response of the endpoint
        response = self.staff_client.get(self.api_users)
        content = json.loads(response.content)
        self.assertEqual(len(content), 0)

    def test_data_cleaned(self):
        self.authenticated_user.allow_sharing_personal_info = False
        self.authenticated_user.save()

        response = self.authenticated_client_2.get(
            self.api_users + "{}/".format(self.authenticated_user.pk)
        )
        content = json.loads(response.content)
        for key in ["username", "first_name", "last_name", "email", "secondary_email"]:
            self.assertIsNone(content[key])

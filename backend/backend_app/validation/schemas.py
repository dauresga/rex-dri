import json
from os.path import join, realpath, dirname

from jsonschema import Draft7Validator

dir_path = dirname(realpath(__file__))
schema_dir = join(dir_path, "json_schemas")

with open(join(schema_dir, "useful_links.json"), "r") as f:
    USEFUL_LINKS_SCHEMA = json.load(f)
    Draft7Validator.check_schema(USEFUL_LINKS_SCHEMA)

with open(join(schema_dir, "photos.json"), "r") as f:
    PHOTOS_SCHEMA = json.load(f)
    Draft7Validator.check_schema(PHOTOS_SCHEMA)

with open(join(schema_dir, "definitions.json"), "r") as f:
    DEFINITIONS_SCHEMA = json.load(f)
    Draft7Validator.check_schema(DEFINITIONS_SCHEMA)

with open(join(schema_dir, "theme.json"), "r") as f:
    THEME_SCHEMA = json.load(f)
    Draft7Validator.check_schema(THEME_SCHEMA)

with open(join(schema_dir, "recommendation_list_content.json"), "r") as f:
    RECOMMENDATION_LIST_CONTENT_SCHEMA = json.load(f)
    Draft7Validator.check_schema(RECOMMENDATION_LIST_CONTENT_SCHEMA)

from django.test import override_settings
from backend_app.tests.utils import WithUserTestCase
from backend_app.permissions.moderation import is_moderation_required
from backend_app.models.for_testing.moderation import ForTestingModeration


class IsModerationActivatedTestCase(WithUserTestCase):
    """
    Test cases for when models have model_moderation level of 1.
    """

    @override_settings(MODERATION_ACTIVATED=True)
    def test_is_moderation_required_staff_1(self):
        self.assertFalse(
            is_moderation_required(ForTestingModeration, None, self.staff_user)
        )
        self.assertFalse(
            is_moderation_required(ForTestingModeration, None, self.staff_user, 3)
        )

    @override_settings(MODERATION_ACTIVATED=False)
    def test_is_moderation_required_staff_2(self):
        self.assertFalse(
            is_moderation_required(ForTestingModeration, None, self.staff_user)
        )
        self.assertFalse(
            is_moderation_required(ForTestingModeration, None, self.staff_user, 3)
        )

    @override_settings(MODERATION_ACTIVATED=True)
    def test_is_moderation_required_lambda_user_1(self):
        self.assertTrue(
            is_moderation_required(ForTestingModeration, None, self.authenticated_user)
        )

    @override_settings(MODERATION_ACTIVATED=False)
    def test_is_moderation_required_lambda_user_2(self):
        self.assertFalse(
            is_moderation_required(ForTestingModeration, None, self.authenticated_user)
        )

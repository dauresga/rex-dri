from backend_app.tests.utils import WithUserTestCase
from backend_app.tests.utils import get_dummy_university


class WriteAccessTestCase(WithUserTestCase):
    """
    Test the write access to DRI restricted elements
    """

    @classmethod
    def setUpMoreTestData(cls):
        cls.univ = get_dummy_university()
        cls.api_dri = "/api/universityDri/?universities={}/".format(cls.univ.pk)
        cls.post_data = dict(
            universities=[cls.univ.pk], title="qsdlkjqsmlkdj", useful_links="[]"
        )

    def test_write_dri_staff(self):
        response = self.staff_client.post(self.api_dri, self.post_data)
        self.assertEqual(response.status_code, 201)

    def test_write_dri_dri(self):
        response = self.dri_client.post(self.api_dri, self.post_data)
        self.assertEqual(response.status_code, 201)

    def test_write_dri_moderator(self):
        response = self.moderator_client.post(self.api_dri, self.post_data)
        self.assertEqual(response.status_code, 403)

    def test_write_dri_authentificated(self):
        response = self.authenticated_client.post(self.api_dri, self.post_data)
        self.assertEqual(response.status_code, 403)

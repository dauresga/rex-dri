from django.test import TestCase
from backend_app.models.abstract.scholarship import ScholarshipSerializer
import pytest
from rest_framework.validators import ValidationError
from backend_app.models.currency import Currency


class ScholarshipTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.EUR = Currency.objects.create(
            code="EUR", name="Euro", one_EUR_in_this_currency=1
        )

    def test_scholarhip_validation(self):
        ser = ScholarshipSerializer()
        attrs = {"useful_links": [], "comment": "", "currency": self.EUR}

        with pytest.raises(ValidationError):
            attrs["amount_min"] = 200
            attrs["amount_max"] = 100
            ser.validate(attrs)

        attrs["amount_min"] = 100
        attrs["amount_max"] = 200
        ser.validate(attrs)

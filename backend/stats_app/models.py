from django.db import models
from django.core.validators import MinValueValidator
from backend_app.models.university import University


class DailyConnections(models.Model):
    date = models.DateTimeField(unique=True, null=False)
    nb_connections = models.IntegerField(validators=[MinValueValidator(0)], null=False)


class DailyExchangeContributionsInfo(models.Model):
    date = models.DateTimeField(null=False)
    TYPE_CHOICES = [
        ("exchange_feedback", "exchange_feedback"),
        ("course_feedback", "course_feedback"),
    ]
    type = models.CharField(max_length=20, choices=TYPE_CHOICES, null=False)
    university = models.ForeignKey(University, null=False, on_delete=models.CASCADE)
    major = models.CharField(max_length=20, null=False, blank=True)
    minor = models.CharField(max_length=47, null=False, blank=True)
    exchange_semester = models.CharField(max_length=5, null=False)
    nb_contributions = models.IntegerField(
        validators=[MinValueValidator(0)], null=False
    )

    class Meta:
        unique_together = (
            "date",
            "type",
            "university",
            "major",
            "minor",
            "exchange_semester",
        )

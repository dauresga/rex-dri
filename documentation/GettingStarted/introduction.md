# Introduction

Welcome on the `Rex-DRI` documentation.

`Rex-DRI` is a plateforme to capitalize on international exchange that outbound _Université de Technologie de Compiègne, France_.

!> This app makes use notably of `Docker`, `Python`, `Django`, `Js`, `React` and `PostgreSQL`.

If you are not familiar with these technologies, have no worries! 😊 You can find very good resources online. For instance:

- Django: [a good tutorial in French](https://tutorial.djangogirls.org/fr/django_start_project/);
- _More to come..._

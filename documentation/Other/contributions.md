# Contributors

?> Thank you to all of those who contributed to this project :thumbsup:

- Solène Aboud
- Ségolène Brisemeur
- Florent Chehab
- Gautier Daures
- Maxime Emschwiller
- Julien Jerphanion
- Alexandre Lanceart
- Imane Misrar
- Estelle Veisemburger

_More information can be found [here](https://gitlab.utc.fr/rex-dri/rex-dri/graphs/master)._

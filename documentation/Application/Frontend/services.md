# Frontend Services

?> _In the contexts of software architecture, service-orientation and service-oriented architecture, the term service refers to a software functionality or a set of software functionalities (such as the retrieval of specified information or the execution of a set of operations) with a purpose that different clients can reuse for different purposes, together with the policies that should control its usage (based on the identity of the client requesting the service, for example). [Wikipedia](https://en.wikipedia.org/wiki/Service_(systems*architecture)*

So the general idea of a _Service_ is to ease the developer experience by providing specialized "Software Blocks".

## Available services in REX-DRI

`REX-DRI` has several of such services, we will describe them here.

### Navigation Service

The purpose of this service is to ease the url / application change.

```jsx
import NavigationService from "...";

function MyComponent() {
  return <button onClick={() => NavigationService.goHome()}>Go Home</button>;
}
```

This service gives you access to the main _routes_ of the application and a direct way to switch between them.

_Check the detailed implementation to see what's possible._

### Notification Service

The purpose of this service is to ease the creation of notifications.

As you can see below, as long as the app is created, you can create notifications super easily.

```jsx
import NotificationService from "...";

function MyComponent({ message }) {
  NotificationService.info(el.message);
  return <></>;
}
```

_Check the detailed implementation to see what's possible._

### FullScreenDialog and Alert Services

Those two services enable the use of the app's FullScreenDialog and Alert component (as there shouldn't be more than one of those at the same time in the app).

```jsx
import FullScreenDialogService from "...";
import AlertService from "...";
```

_Check the detailed implementation to see what's possible._

### Filter Service

Well, this one is to handle university filtering logic outside of the components.

### Data Services

There are also several "Data Services":

- CityService
- CountryService
- CurrencyService
- LanguageService
- UniversityService

Those services should be used when you need to access this type of general data: they centralize conversions, joins, access by id, etc. So you can easily manipulate this data directly in the frontend / browser.

## How those services are initialized

### UI Services

Most "UI Services" have their matching react component counterpart: `...ServiceComponent`. They are mounted and initialized [as soon the App](https://gitlab.utc.fr/rex-dri/rex-dri/-/blob/dev/frontend/src/entry/mainApp.jsx#L68) is created.

### Data Services

"Data services" are initialized as soon as the [`App` component](https://gitlab.utc.fr/rex-dri/rex-dri/-/blob/dev/frontend/src/components/app/App.jsx#L50) is loaded (and the necessary data has been loaded from the REST API).

# Linking Universities

## Problem description

Data from UTC DSI is automatically updated every day.
However, we need to link new university data from the DSI with universities on REX-DRI to guarantee data integrity (there might be duplicates on the DSI side) and correctness. Unlinked universities ids and names are displayed on this route: [https://rex.dri.utc.fr/app/about/unlinked-universities](https://rex.dri.utc.fr/app/about/unlinked-universities).

## Linking (DSI) partners and (REX-DRI) universities

There are two cases:

1. The university (on REX-DRI) already exists. Go to [https://rex.dri.utc.fr/admin](https://rex.dri.utc.fr/admin), select the Partner object with the correct id. Then link it to the right university object.

2. The university does not exist. Go to [https://rex.dri.utc.fr/admin](https://rex.dri.utc.fr/admin) create a new University object, with all its information (website, coordinates, etc.). Then select the Partner object with the corresponding id and link it with the university just created (just like in case 1.).

## Updating denormalized information

After you have done either 1. or 2., you **must** update the denormalized information on the universities. Administrators can do it from the trigger_cron admin page: [https://rex.dri.utc.fr/admin/trigger_cron](https://rex.dri.utc.fr/admin/trigger_cron). Click on update extra denormalization to update information about universities.

# Releasing

## Creating a release (git)

Step by step, how to create a release:

- checkout from `dev`
- update the changelog to reflect the release date, etc.
- commit
- create a merge request into `dev`
- merge it
- create a merge request from `dev` in `master`
- then merge it
- tag the release commit appropriately
- You are good to go :tada:

## Deploying (for UTC)

?> Before doing so you should have added a `Site Information` from [the website admin](https://rex.dri.utc.fr/admin/base_app/siteinformation/) as there is gonna be some downtime.

### Step-by-step

Connect to the REX-DRI VM

Connect to the REX-DRI user:

```bash
sudo su - rex-dri
```

!> :warning: In the current setup, you need to sudo every command

- Down the website

```bash
cd ~/apps/rex-dri
make down_prod
```

- Clean the useless docker volumes

```bash
# Check if you have randomly named volumes:
docker volume ls
# rm ONLY the one with random names
# docker volume rm ...
```

- It's always a good idea to keep a VM up-to-date

```bash
service docker stop
apt udpate
apt upgrade
service docker start
```

- Restart the API on top of the DSI data base

```bash
cd ~/apps/api-db-utc
make prod
# Check it's running
curl localhost:8083/api/courses/chehabfl
```

- Update the rex-dri repository

```bash
cd ~/apps/rex-dri
git pull
# You should be on the master branch
git status
```

- Restart the website

```bash
# this will take several minutes to execute
# the frontend is going to be built, etc.
make prod
```

- Update the remote data (to check that this important part works)

```bash
sudo make shell_backend_prod
# then
./manage.py update_external_data all
```

- Clean the unused docker images (if a new one was pulled)

```bash
docker images
# rm the new unused images
# docker rmi
```

### Post deployment

- [ ] Make sure auto-update works the next day
- [ ] Check `/app/my/exchanges/` reload button is working

## Deploying (other)

### Step-by-step

Deploying the app is fairly simple: all you need is `docker` and `docker-compose` installed.

If you don't know how to install them, you should look in [this section](GettingStarted/set-up.md) of the documentation.

- Once this is done clone the repository of the project.
- Then, run `make setup` (this will generate the required `.env` files) and set their correct values in both `server/envs` directory and `.env` (at the root of the repo -- for the map to work appropriately). Take care especially with the files `django.env` and `external_data.env`.
- Once this is done, you can simply run `make prod` and then `make init_dev_data` (TODO create one for prod) to get going fast.

`make prod` will start all the services described in `docker-compose.prod.yml` (in the `server` directory).

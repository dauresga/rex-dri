# In CI we use the latest docker images.

stages:
  - check_and_lint
  - test
  - svg-gen-docu # required to be done before documentation and in separate stages
  - documentation

.only-default: &only-default
  only:
    - merge_requests
    - dev
    - master

check_back:
  <<: *only-default
  stage: check_and_lint
  image: registry.gitlab.utc.fr/rex-dri/rex-dri/backend:v0.2.1
  before_script:
    - sh ./backend/init_logs.sh
    - make setup
  script:
    - cd backend && ./manage.py check
    - cd ../documentation && make extract_django # Try to generate .dot files for the system architecture
  artifacts:
    paths:
      - documentation/generated/
    expire_in: 1 hour
  variables:
    FIXER_API_TOKEN: 91ed43e97a55f9ed9a501cc005c15e9c
    UTC_API_ENDPOINT: http://192.168.122.1:8083/api
  tags:
    - docker

check_front:
  <<: *only-default
  stage: check_and_lint
  image: registry.gitlab.utc.fr/rex-dri/rex-dri/frontend:v2.1.1
  before_script:
    - cd frontend && mkdir -p node_modules && mv -f /usr/src/deps/node_modules/* /usr/src/deps/node_modules/.bin ./node_modules/
  script:
    - yarn build
  artifacts:
    paths:
      - frontend/webpack-stats.json
    expire_in: 1 hour
  tags:
    - docker

lint_backend:
  <<: *only-default
  stage: check_and_lint
  image: registry.gitlab.utc.fr/rex-dri/rex-dri/backend:v0.2.1
  script:
    - cd backend
    - black --check .
    - flake8
  tags:
    - docker

lint_frontend_and_documentation:
  <<: *only-default
  stage: check_and_lint
  image: registry.gitlab.utc.fr/rex-dri/rex-dri/frontend:v2.1.1
  before_script:
    - cd frontend && mkdir -p node_modules && mv -f /usr/src/deps/node_modules/* /usr/src/deps/node_modules/.bin ./node_modules/
  script:
    - yarn lint
    - yarn lint-doc
  tags:
    - docker

test_back:
  <<: *only-default
  stage: test
  image: registry.gitlab.utc.fr/rex-dri/rex-dri/backend:v0.2.1
  variables:
    POSTGRES_DB: postgres
    POSTGRES_USER: postgres
    POSTGRES_PASSWORD: postgres
    POSTGRES_HOST: postgres
    POSTGRES_PORT: 5432 # We absolutely need this one since a gitlab runner will inject a similar variable; will cause tests to fail.
    FIXER_API_TOKEN: 91ed43e97a55f9ed9a501cc005c15e9c
    UTC_API_ENDPOINT: http://192.168.122.1:8083/api
  services:
    - postgres:10.5
  before_script:
    - sh ./backend/init_logs.sh
    - make setup
  script:
    - cd backend
    - pytest base_app/ backend_app/ stats_app/ --cov --cov-config .coveragerc --cov-report term --cov-report html
  artifacts:
    paths:
      - backend/htmlcov/
    expire_in: 1 hour
  tags:
    - docker

test_frontend:
  <<: *only-default
  stage: test
  image: registry.gitlab.utc.fr/rex-dri/rex-dri/frontend:v2.1.1
  before_script:
    - cd frontend && mkdir -p node_modules && mv -f /usr/src/deps/node_modules/* /usr/src/deps/node_modules/.bin ./node_modules/
  script:
    - yarn test
  tags:
    - docker

generate_UML_svg:
  <<: *only-default
  stage: svg-gen-docu
  image: floawfloaw/plantuml
  script: cd documentation && make convert_to_svg
  dependencies:
    - check_back
  artifacts:
    paths:
      - documentation/generated/
    expire_in: 1 hour
  tags:
    - docker

pages:
  stage: documentation
  image: floawfloaw/plantuml
  dependencies:
    - test_back
    - generate_UML_svg
  script:
    - mkdir .public
    - mv backend/htmlcov/ .public/coverage
    - mv documentation/ .public/documentation
    - mv .public public
  artifacts:
    paths:
      - public
    expire_in: 1 month
  only:
    - dev
  tags:
    - docker
